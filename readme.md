This program acts a standin for moneris credit card verification server for testing purposes.

To Build:

Install haskell platform

at command line run:

	cabal install scotty 
	cabal install blaze-html
	ghc -O2 VchMonerisVerificationFaker.hs

Can be run interpreted like this:

	runghc VchMonerisVerificationFaker.hs