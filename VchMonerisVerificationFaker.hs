{-# LANGUAGE OverloadedStrings #-}
import Web.Scotty
import Network.Wai.Middleware.RequestLogger
import Control.Monad.Trans
import Data.Monoid
import qualified Text.Blaze.Html5 as H
import Network.HTTP.Types (status302)
import Network.Wai
import qualified Data.Text.Lazy as T
import Data.Text.Lazy.Encoding (decodeUtf8)
import Text.Blaze.Html5.Attributes
import Text.Blaze.Html.Renderer.Text (renderHtml)
import Data.Default (def)

opts :: Options
opts = def { verbose = 0 }
           
main :: IO ()
main = scottyOpts opts $ do    
    middleware logStdoutDev
    liftIO $ putStrLn "Moneris Verification faker\n"
    liftIO $ putStrLn "Go to http://localhost:3000/verify in browser and leave this running\n"
    get "/verify" $ do
        html $ renderHtml
             $ H.html $ do
                H.body $ do
                    H.h1 "Moneris Verification faker"
                    H.p $ "This process runs on port 3000 and generates fake moneris verifications.  This might be needed if moneris verifications are failing and would prefer to verify by hand"
                    H.p $ "Set your verification url to http://localhost:3000/verify"
                    H.p $ "Also, you can try it by entering a store_id / hpp_key / transaction number below and submitting"                       
                    H.form H.! method "post" H.! action "/verify" $ do
                        H.input H.! type_ "text" H.! name "ps_store_id"
                        H.input H.! type_ "text" H.! name "hpp_key"
                        H.input H.! type_ "text" H.! name "transactionKey"
                        H.input H.! type_ "submit"
    post "/verify" $ do
        -- ps_store_id <- param "ps_store_id"
        -- hpp_key <- param "hpp_key"
        transactionKey <- param "transactionKey"
        html $ template transactionKey
        
template :: T.Text -> T.Text
template key = mconcat [
            "<?xml version=\"1.0\" standalone=\"yes\"?>",
            "<response>",
            "<order_id>mhp3234033470</order_id>",
            "<response_code>27</response_code>",
            "<amount>0.00</amount>",
            "<txn_num>679370-0_7</txn_num>",
            "<status>Valid-Approved</status>",
            "<transactionKey>",
            key,
            "</transactionKey>",
            "</response>"]